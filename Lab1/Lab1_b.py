import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch



def qOdleglosc(qp, qk):
    return np.sqrt((qp[0] - qk[0])**2 + (qp[1] - qk[1])**2)


def Up(qr, qk, kp):
    wynik = 0.5*kp*(qOdleglosc(qr, qk)**2)
    return wynik

def Voi(qr, qoi, d0, koi):
    if qOdleglosc(qr,qoi) <= d0:
        wynik = 0.5*(koi*(1/qOdleglosc(qr,qoi) - 1/d0)**2)
    else:
        wynik = 0

    return wynik

def Fp(qr, qk, kp):
    wynik = kp*qOdleglosc(qr, qk)
    return wynik


def Foi(qr, qoi, koi, d0):
    a = qOdleglosc(qr, qoi)
    if qOdleglosc(qr, qoi) <= d0:
        if qOdleglosc(qr, qoi) == 0:
            return 1000
        else:
            return koi*(1/qOdleglosc(qr, qoi) - 1/d0)*(1/(qOdleglosc(qr, qoi)**2))
    else:
        return 0

def SumFoi(qr, qo, ko = 1, d0= 20):
    Sum = 0
    for i in qo:
        Sum += (Foi(qr, i, ko, d0))
    return Sum


def MacierzSil(qo, qk, Z, d0 = 20, kp = 0.5, ko = 1):

    l = 0;

    for i in np.arange(-10, 10, delta):
        p = 0;
        for j in np.arange(-10, 10, delta):

            Z[l][p] = Fp((j,i), qk, kp) + SumFoi((j,i), qo, ko , d0)
            p = p+1

        l = l+1


    return Z



delta = 0.1 #0.01
x_size = 10
y_size = 10

y_start = np.random.randint(-10, 10)
y_stop = np.random.randint(-10, 10)

x_prze1 = np.random.randint(-10, 10);
y_prze1 = np.random.randint(-10, 10);

x_prze2 = np.random.randint(-10, 10);
y_prze2 = np.random.randint(-10, 10);

x_prze3 = np.random.randint(-10, 10);
y_prze3 = np.random.randint(-10, 10);

x_prze4 = np.random.randint(-10, 10);
y_prze4 = np.random.randint(-10, 10);

obst_vect = [(x_prze1, y_prze1), (x_prze2, y_prze2), (x_prze3, y_prze3), (x_prze4, y_prze4)]
start_point=(-10,y_start)
finish_point=(10,y_stop)



x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)


#--------------------------Rozne d0----------------------------------------:

Z = MacierzSil(obst_vect, finish_point, Z, d0=1)


fig = plt.figure(figsize=(10, 12))
ax = fig.add_subplot(221)
ax.set_title('Metoda potencjałów d0=1, kp=0.5, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=5)


ax = fig.add_subplot(222)
ax.set_title('Metoda potencjałów d0=5, kp=0.5, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=10)


ax = fig.add_subplot(223)
ax.set_title('Metoda potencjałów d0=10, kp=0.5, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20)


ax = fig.add_subplot(224)
ax.set_title('Metoda potencjałów d0=20, kp=0.5, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


#--------------------------------Rozne kp-----------------------------------:

Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 0.5)


fig = plt.figure(figsize=(10, 12))
ax = fig.add_subplot(221)
ax.set_title('Metoda potencjałów d0=20, kp=0.5, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 0.25)


ax = fig.add_subplot(222)
ax.set_title('Metoda potencjałów d0=20, kp=0.25, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=7, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 1)


ax = fig.add_subplot(223)
ax.set_title('Metoda potencjałów d0=20, kp=1, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=20, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 2)


ax = fig.add_subplot(224)
ax.set_title('Metoda potencjałów d0=20, kp=2, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=40, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


#--------------------------------Rozne ko-----------------------------------:

Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 0.5, ko = 1)


fig = plt.figure(figsize=(10, 12))
ax = fig.add_subplot(221)
ax.set_title('Metoda potencjałów d0=20, kp=0.5, ko=1')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 0.5, ko = 0.25)


ax = fig.add_subplot(222)
ax.set_title('Metoda potencjałów d0=20, kp=0.5, ko=0.5')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 0.5, ko = 4)


ax = fig.add_subplot(223)
ax.set_title('Metoda potencjałów d0=20, kp=1, ko=2')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


Z = MacierzSil(obst_vect, finish_point, Z, d0=20, kp = 0.5, ko = 10)


ax = fig.add_subplot(224)
ax.set_title('Metoda potencjałów d0=20, kp=2, ko=5')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=14, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.colorbar(orientation='vertical')
plt.grid(True)


plt.show()

