import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import sys


def qOdleglosc(qp, qk):
    return np.sqrt((qp[0] - qk[0])**2 + (qp[1] - qk[1])**2)


def Up(qr, qk, kp):
    wynik = 0.5*kp*(qOdleglosc(qr, qk)**2)
    return wynik

def Voi(qr, qoi, d0, koi):
    if qOdleglosc(qr,qoi) <= d0:
        wynik = 0.5*(koi*(1/qOdleglosc(qr,qoi) - 1/d0)**2)
    else:
        wynik = 0

    return wynik

def Fp(qr, qk, kp):
    wynik = kp*qOdleglosc(qr, qk)
    return wynik

def Foi(qr, qoi, koi, d0):
    if qOdleglosc(qr, qoi) <= d0:
        if qOdleglosc(qr, qoi) == 0:
            return 10
        else:
            return koi*(1/qOdleglosc(qr, qoi) - 1/d0)*(1/(qOdleglosc(qr, qoi)**2))
    else:
        return 0

def SumFoi(qr, qo, ko, d0):
    Sum = 0
    for i in qo:
        Sum += (Foi(qr, i, ko, d0))
    return Sum


def MacierzSil(qo, qk, Z, d0=20, ko=1, kp=1):

    l = 0

    for i in np.arange(-10, 10, delta):
        p = 0
        for j in np.arange(-10, 10, delta):

            Z[l][p] = Fp((j,i), qk, kp) + SumFoi((j,i), qo, ko , d0)
            p = p+1

        l = l+1


    return Z

def SilyWokol(qr, qo, qk, d0, ko, kp):

    WartoscSil = np.zeros((3, 3))


    for i in range(0, 3):
        for j in range(0, 3):
            WartoscSil[i][j] = Fp((qr[0] + i*delta - delta, qr[1] + j*delta - delta), qk, kp) + SumFoi((qr[0] + i*delta - delta, qr[1] + j*delta - delta), qo, ko, d0)


    if qr[0] <= -10:
        if qr[1] <= -10:
            WartoscSil[2][0] = 0
            WartoscSil[2][1] = 0
            WartoscSil[2][2] = 0
            WartoscSil[0][2] = 0
            WartoscSil[1][2] = 0
        elif qr[1] >= 10:
            WartoscSil[2][0] = 0
            WartoscSil[1][0] = 0
            WartoscSil[0][0] = 0
            WartoscSil[2][1] = 0
            WartoscSil[2][2] = 0
        else:
            WartoscSil[2][0] = 0
            WartoscSil[2][1] = 0
            WartoscSil[2][2] = 0

    elif qr[0] >= 10:
        if qr[1] <= -10:
            WartoscSil[0][0] = 0
            WartoscSil[0][1] = 0
            WartoscSil[0][2] = 0
            WartoscSil[1][2] = 0
            WartoscSil[2][2] = 0
        elif qr[1] >= 10:
            WartoscSil[0][0] = 0
            WartoscSil[1][0] = 0
            WartoscSil[2][0] = 0
            WartoscSil[0][1] = 0
            WartoscSil[0][2] = 0
        else:
            WartoscSil[0][0] = 0
            WartoscSil[0][1] = 0
            WartoscSil[0][2] = 0

    else:
        if qr[1] <= -10:
            WartoscSil[0][2] = 0
            WartoscSil[1][2] = 0
            WartoscSil[2][2] = 0
        elif qr[1] >= 10:
            WartoscSil[0][0] = 0
            WartoscSil[1][0] = 0
            WartoscSil[2][0] = 0

    #print(WartoscSil)
    return WartoscSil

def SilaWypadkowa(WartoscSil):

    x = WartoscSil[0][1] * np.cos(0)
    x = x + WartoscSil[0][0] * np.cos(np.pi / 4)
    x = x + WartoscSil[1][0] * np.cos(np.pi / 2)
    x = x + WartoscSil[2][0] * np.cos(3 * np.pi / 4)
    x = x + WartoscSil[2][1] * np.cos(np.pi)
    x = x + WartoscSil[2][2] * np.cos(5 * np.pi / 4)
    x = x + WartoscSil[1][2] * np.cos(3 * np.pi / 2)
    x = x + WartoscSil[0][2] * np.cos(7 * np.pi / 4)

    y = WartoscSil[0][1] * np.sin(0)
    y = y + WartoscSil[0][0] * np.sin(np.pi / 4)
    y = y + WartoscSil[1][0] * np.sin(np.pi / 2)
    y = y + WartoscSil[2][0] * np.sin(3 * np.pi / 4)
    y = y + WartoscSil[2][1] * np.sin(np.pi)
    y = y + WartoscSil[2][2] * np.sin(5 * np.pi / 4)
    y = y + WartoscSil[1][2] * np.sin(3 * np.pi / 2)
    y = y + WartoscSil[0][2] * np.sin(7 * np.pi / 4)

    kat = np.arctan2(y, x)
    #print("kat :", kat * 180 / np.pi)

    if kat < 0:
        kat += 2*np.pi

    #print("kat :", kat * 180 / np.pi)

    if kat > 15*np.pi/8 or kat <= np.pi/8:
        return (1*delta, 0)
    if kat > np.pi/8 and kat <= 3*np.pi/8:
        return (1*delta, 1*delta)
    if kat > 3*np.pi/8 and kat <= 5*np.pi/8:
        return (0, 1*delta)
    if kat > 5*np.pi/8 and kat <= 7*np.pi/8:
        return (-1*delta, 1*delta)
    if kat > 7*np.pi/8 and kat <= 9*np.pi/8:
        return (-1*delta, 0)
    if kat > 9*np.pi/8 and kat <= 11*np.pi/8:
        return (-1*delta, -1*delta)
    if kat > 11*np.pi/8 and kat <= 13*np.pi/8:
        return (0, -1*delta)
    if kat > 13*np.pi/8 and kat <= 15*np.pi/8:
        return (1*delta, -1*delta)


def Droga(qr, qo, qk, Sciezka, licznik_rekurencji, d0=20, ko=1, kp=1 ):
    global Robot_zablokowany

    ruch = SilaWypadkowa(SilyWokol(qr, qo, qk, d0, ko, kp))
    nowa_pozycja_x = qr[0] + ruch[0]
    nowa_pozycja_y = qr[1] + ruch[1]

    qr_nowa = (nowa_pozycja_x, nowa_pozycja_y)
    Sciezka.append(qr_nowa)

    #Warunek koncowy odporny na bledy numeryczne
    if qOdleglosc(qr_nowa, qk) < delta:
        return
    else:
        if licznik_rekurencji >= (sys.getrecursionlimit()-10):
            Robot_zablokowany = 1
            return

        #assert licznik_rekurencji < (sys.getrecursionlimit()-10), "Zbyt duzo rekurencji, czy robot sie zablokowal ?"
        licznik_rekurencji = licznik_rekurencji + 1
        Droga(qr_nowa, qo, qk, Sciezka, licznik_rekurencji, d0, ko, kp)

delta = 0.1 #0.01
x_size = 10
y_size = 10

y_start = np.random.randint(-10, 10)
y_stop = np.random.randint(-10, 10)

x_prze1 = np.random.randint(-10, 10);
y_prze1 = np.random.randint(-10, 10);

x_prze2 = np.random.randint(-10, 10);
y_prze2 = np.random.randint(-10, 10);

x_prze3 = np.random.randint(-10, 10);
y_prze3 = np.random.randint(-10, 10);

x_prze4 = np.random.randint(-10, 10);
y_prze4 = np.random.randint(-10, 10);

obst_vect = [(x_prze1, y_prze1), (x_prze2, y_prze2), (x_prze3, y_prze3), (x_prze4, y_prze4)]
start_point=(-10,y_start)
finish_point=(10,y_stop)

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)


d0 = 20
ko = 20
kp = 1

Z = MacierzSil(obst_vect, finish_point, Z, d0, ko, kp)

Sciezka =[]
licznik_rekurencji = 0
Robot_zablokowany = 0

Droga(start_point, obst_vect, finish_point, Sciezka, licznik_rekurencji, d0, ko, kp)


d0 = 5
Sciezka1 =[]
licznik_rekurencji = 0
Droga(start_point, obst_vect, finish_point, Sciezka1, licznik_rekurencji, d0, ko, kp)

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)

if Robot_zablokowany == 1:
    ax.text(-4, 0, 'Robot się zablokował !!!', style='italic',
            bbox={'facecolor': 'red', 'alpha': 0.5, 'pad': 10})

ax.set_title('Metoda potencjałów')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size],
           vmax=30, vmin=-1)


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

plt.plot([i[0] for i in Sciezka], [i[1] for i in Sciezka], color='green', label='Sciezka d0 = 20')
plt.plot([i[0] for i in Sciezka1], [i[1] for i in Sciezka1], color='red', label='Sciezka d0 = 5')

plt.legend()
plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()

